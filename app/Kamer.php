<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Kamer extends Model
{
    protected $fillable = [
        "id",
        "number",
        "location_id",
        "name"
    ];

    public function location()
    {
        return $this->belongsTo('App\Locatie', 'location_id');
    }

    public function locationLocal()
    {
        return $this->belongsTo('App\Locatie', 'location_id')->where('id', Auth::user()->id);
    }

    public function bewoner(){
        return $this->hasOne('App\Bewoner', 'room_id');
    }
}
