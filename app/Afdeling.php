<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Afdeling extends Model
{
    protected $table = 'afdelingen';

    public function location()
    {
        return $this->belongsTo('App\Locatie', 'location_id');
    }

    public function ruimtes()
    {
        return $this->hasMany('App\Ruimte', 'department_id');
    }
}
