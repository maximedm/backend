<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hulpmiddel extends Model
{
    protected $table = 'hulpmiddelen';

    protected $fillable = [
        "path",
        "name",
        "description"
    ];
}
