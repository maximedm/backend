<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bewoner extends Model
{
    protected $fillable = [
        "firstname",
        "lastname",
        "room_id",
        'uuid',
        "breakfast",
        "lunch",
        "dinner",
        "breakfast_hulpmiddelen",
        "lunch_hulpmiddelen",
        "dinner_hulpmiddelen",
        "breakfast_seat",
        "lunch_seat",
        "dinner_seat",
        "table",
        "notes"
    ];

    public function room()
    {
        return $this->belongsTo('App\Kamer', 'room_id');
    }
}

