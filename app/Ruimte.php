<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruimte extends Model
{
    //
    public function afdeling()
    {
        return $this->belongsTo('App\Afdeling', 'department_id');
    }
}
