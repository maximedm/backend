<?php

namespace App\Http\Controllers;

use App\Afdeling;
use App\Hulpmiddel;
use App\Locatie;
use App\Ruimte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RuimteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$ruimtes = Ruimte::all();
        $ruimtes = DB::table('ruimtes')->join('afdelingen', 'afdelingen.id', '=', 'ruimtes.department_id')->join('locaties', 'locaties.id', '=', 'afdelingen.location_id')->select('ruimtes.*', 'afdelingen.name as afdelingname')->where('locaties.id', Auth::user()->location)->get();
        $afdelingen = Afdeling::where('location_id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $ruimtes = Ruimte::all();
            $afdelingen = Afdeling::all();
        }

        if($afdelingen->count() == 0){
            return redirect('/afdelingen')->with('error', "Gelieve eerst een <b>afdeling</b> toe te voegen.");
        }
        return view('frontend.ruimte.index', compact('ruimtes', 'afdelingen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locaties = Locatie::all();
        $afdelingen = Afdeling::all();
        return view('frontend.ruimte.create', compact('locaties', 'afdelingen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //Hulpmiddel::created($request->all());

        $ruimte = new Ruimte;
        $ruimte->name = $request->name;
        $ruimte->department_id = $request->locatie;
        $ruimte->save();

        return redirect('/ruimte');
    }

    public function edit($ruimte){
        $item = Ruimte::where('id', $ruimte)->first();
        $afdelingen = Afdeling::where('location_id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $afdelingen = Afdeling::all();
        }

        return view('frontend.ruimte.edit', compact('item', 'afdelingen'));
    }

    public function updateEdit(Request $request){
        $item = Ruimte::find($request->id)->first();

        $item->name              = $request->name;
        $item->department_id     = $request->locatie;
        $item->save();

        return redirect('/ruimte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamer  $kamer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Hulpmiddel::find($request->id)->delete();
        return back();
    }

    public function schets($ruimte){
        $r = Ruimte::where('id', $ruimte)->first();
        $afdeling = $r->department_id;
        $woonzorgcentrum = $r->afdeling->location_id;

        return view('frontend.schetsmodule.index', compact('ruimte', 'afdeling', 'woonzorgcentrum'));
    }
}
