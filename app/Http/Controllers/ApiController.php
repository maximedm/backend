<?php

namespace App\Http\Controllers;

use App\Afdeling;
use App\Bewoner;
use App\Hulpmiddel;
use App\Locatie;
use App\Ruimte;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function locaties(){
        $locaties = Locatie::all()->toJson();
        return $locaties;
    }

    public function bewoners($locatie){
        $res = DB::table('bewoners')->join('kamers', 'bewoners.room_id', 'kamers.id')->where('kamers.location_id', $locatie)->select('bewoners.*', 'kamers.number')->get()->toJson();
        return $res;
    }

    public function updateUUID(Request $request){
        $bewoner = Bewoner::where('id', $request->user)->first();
        //return $request->all();
        try {
            $bewoner->uuid = $request->uuid;
            $bewoner->save();
            return $bewoner;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function userInfoList($locatie){
        $hour = Carbon::now('CEST')->addHours('1')->format('H');
        $bewoner = DB::table('bewoners')->join('kamers', 'bewoners.room_id', 'kamers.id')->where('kamers.location_id', $locatie)->select('bewoners.*', 'kamers.number')->get()->toJson();
        //dd($bewoner);

        if($hour <= 10){
            return $bewoner;
        }
        if($hour > 10 && $hour < 16){
            return $bewoner;
        }
        if($hour >= 15){
            return $bewoner;
        }
    }

    public function hulpmiddelen(){
        $hulpmiddelen = Hulpmiddel::all();
        $data = collect();

        foreach($hulpmiddelen as $hulpmiddel){
            $data[$hulpmiddel->id] = [
                'path'          => $hulpmiddel->path,
                'name'          => $hulpmiddel->name,
                'description'   => $hulpmiddel->description
            ];
        }

        return $data->toJson();
    }

    public function eetruimteUpdate(Request $request){
        try{
            //return $request->bewoners;
            $ruimte = Ruimte::where('id', $request->id)->first();
            $ruimte->tafels = $request->tafels;
            $ruimte->bewoners = $request->bewoners;
            $ruimte->opmerkingen = $request->opmerkingen;
            $ruimte->save();
            return $ruimte;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function eetruimteFind($ruimte){
        try {
            $item = Ruimte::where('id', $ruimte)->first();
            $data = collect([
                'id'            => $item->id,
                'name'          => $item->name,
                'bewoners'      => $item->bewoners,
                'tafels'        => $item->tafels,
                'opmerkingen'   => $item->opmerkingen

                /*'bewoners'      => str_replace('"draggable":true,', '', $item->bewoners),
                'tafels'        => str_replace('"draggable":true,', '', $item->tafels),
                'opmerkingen'   => str_replace('"draggable":true,', '', $item->opmerkingen)*/

            ]);

            return $data->toJson();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function bewonerInfo(Bewoner $bewoner){
        return $bewoner->toJson();
    }

    public function afdelingenList(Locatie $locatie){
        $afdelingen = Afdeling::where('location_id', $locatie->id)->get();
        return $afdelingen->toJson();
    }

    public function ruimteList(Afdeling $afdeling){
        try {
            $ruimtes = $afdeling->ruimtes;

            $data = collect();
            $i = 0;
            foreach($ruimtes as $ruimte){
                $data[$i++] = [
                    'id'            => $ruimte->id,
                    'name'          => $ruimte->name,
                    'bewoners'      => str_replace('"draggable":true,', '', $ruimte->bewoners),
                    'tafels'        => str_replace('"draggable":true,', '', $ruimte->tafels),
                    'opmerkingen'   => str_replace('"draggable":true,', '', $ruimte->opmerkingen)
                ];
            }
            return $data->toJson();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
