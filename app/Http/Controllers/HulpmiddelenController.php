<?php

namespace App\Http\Controllers;

use App\Bewoner;
use App\Hulpmiddel;
use App\Kamer;
use App\Locatie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class HulpmiddelenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hulpmiddelen = Hulpmiddel::all();
        return view('frontend.hulpmiddelen.index', compact('hulpmiddelen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.hulpmiddelen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $hulpmiddel = new Hulpmiddel;
        if($request->file('path')) {
            $pic = $request->file('path');
            $extension = $pic->getClientOriginalExtension();
            Storage::disk('public')->put($pic->getFilename() . '_' . time() . '.' . $extension, File::get($pic));
            $file = $pic->getFilename() . '_' . time() . '.' . $extension;

            $hulpmiddel->path = $file;
        }


        $hulpmiddel->name = $request->name;

        $hulpmiddel->description = $request->description;
        $hulpmiddel->save();

        return redirect('/hulpmiddelen');
    }

    public function edit(Hulpmiddel $hulpmiddel){
        return view('frontend.hulpmiddelen.edit', compact('hulpmiddel'));
    }

    public function update(Request $request){
        $hulpmiddel = Hulpmiddel::where('id', $request->id)->first();

        if($request->file('path')) {
            $pic = $request->file('path');
            $extension = $pic->getClientOriginalExtension();
            Storage::disk('public')->put($pic->getFilename() . '_' . time() . '.' . $extension, File::get($pic));
            $file = $pic->getFilename() . '_' . time() . '.' . $extension;

            $hulpmiddel->path = $file;
        }

        $hulpmiddel->name = $request->name;

        $hulpmiddel->description = $request->description;
        $hulpmiddel->save();


        return redirect('/hulpmiddelen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamer  $kamer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Hulpmiddel::find($request->id)->delete();
        return back();
    }
}
