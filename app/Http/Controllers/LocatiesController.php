<?php

namespace App\Http\Controllers;

use App\Locatie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Location;
use Str;

class LocatiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $locaties = Locatie::all();
        return view('frontend.locaties.index', compact('locaties'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $loc = new Locatie;
        $loc->name              = $request->name;
        $loc->city              = $request->city;
        $loc->admin_mail        = Str::slug($request->name) . '@happ.be';
        $loc->admin_password    = Hash::make($request->admin_password);
        $loc->user_password     = Hash::make($request->user_password);
        $loc->save();

        $user = new User();
        $user->username         = $request->name;
        $user->location         = $loc->id;
        $user->email            = Str::slug($request->name) . '@happ.be';
        $user->password         = Hash::make($request->admin_password);
        $user->password_user    = Hash::make($request->user_password);
        $user->save();

        return redirect('/locaties');
    }

    public function edit(Locatie $locatie){
        return view('frontend.locaties.edit', compact('locatie'));
    }

    public function update(Request $request){
        $loc = Locatie::find($request->id)->first();

        $loc->name              = $request->name;
        $loc->city              = $request->city;
        $loc->save();


        $user = User::where('email', $request->admin_email)->first();
        if($request->admin_password){
            $user->password         = Hash::make($request->admin_password);
        }
        if($request->password_user){
            $user->password_user    = Hash::make($request->password_user);
        }
        $user->save();
        return redirect('/locaties');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request)
    {
        Locatie::find($request->id)->delete();
        return back();
    }

}
