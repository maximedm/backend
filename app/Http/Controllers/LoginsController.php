<?php

namespace App\Http\Controllers;

use App\Afdeling;
use App\Locatie;
use App\Ruimte;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class LoginsController extends Controller
{
    public function werknemer(){
        $locaties = Locatie::all();
        return view('frontend.login.werknemer', compact('locaties'));

    }

    public function werknemerLogin(Request $request){
        $user = User::where('location', $request->location)->first();
        //dd(Hash::check($request->password, $user->password_user));
        if($user && Hash::check($request->password, $user->password_user)){
            //Session::put('location', $user->location);
            Cookie::queue('location', $user->location, 4500);
            return redirect('/indeling');
        }
        return back()->with('error', 'Het wachtwoord was foutief');
    }


    public function overview(){

        $locatie = Locatie::where('id', Cookie::get('location'))->first();
        $afdelingen = Afdeling::where('location_id', $locatie->id)->get();
        if($locatie){
            return view('frontend.werknemer.index')->with(
                ['afdelingen' => $afdelingen]
            );
        }


    }

    public function indeling(){
        \Debugbar::disable();
        if(Cookie::get('location')){
            return view('frontend.werknemer.indeling');
        } else {
            return redirect('/werknemer');
        }
    }

    public function beheerder(){
        $locaties = Locatie::all();

        return view('frontend.login.beheerder', compact('locaties'));
    }

    public function beheerderLogin(Request $request){
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect('/');
        }
        return back()->with('error', 'Het wachtwoord was foutief');

    }

    public function logout(){
        dd('test');
        Auth::logout();
        return redirect('/beheerder');
    }
}
