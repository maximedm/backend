<?php

namespace App\Http\Controllers;

use App\Afdeling;
use App\Bewoner;
use App\Hulpmiddel;
use App\Locatie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        $bewoners = Bewoner::all()->count();
        $hulpmiddelen = Hulpmiddel::all()->count();
        $afdelingen = Afdeling::all()->count();
        $woonzorgcentra = Locatie::all()->count();
        return view('dashboard', compact('bewoners', 'hulpmiddelen', 'afdelingen', 'woonzorgcentra'));
    }
}
