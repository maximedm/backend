<?php

namespace App\Http\Controllers;

use App\Afdeling;
use App\Hulpmiddel;
use App\Locatie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AfdelingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $afdelingen = Afdeling::where('location_id', Auth::user()->location)->get();
        $locaties = Locatie::where('id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $afdelingen = Afdeling::all();
            $locaties = Locatie::all();
        }
        if($locaties->count() == 0){
            return redirect('/locaties')->with('error', "Gelieve eerst een <b>woonzorgcentrum</b> toe te voegen.");
        }
        return view('frontend.afdeling.index', compact('afdelingen', 'locaties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locaties = Locatie::all();
        return view('frontend.afdeling.create', compact('locaties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //Hulpmiddel::created($request->all());

        $hulpmiddel = new Afdeling;
        $hulpmiddel->name = $request->name;
        if(Auth::user()->location == 0){
            $hulpmiddel->location_id = $request->locatie;
        } else {
            $hulpmiddel->location_id = Auth::user()->location;
        }

        $hulpmiddel->save();

        return redirect('/afdelingen');
    }

    public function edit(Afdeling $afdeling){
        return view('frontend.afdeling.edit', compact('afdeling'));
    }

    public function update(Request $request){
        $item = Afdeling::find($request->id)->first();

        $item->name              = $request->name;
        $item->save();

        return redirect('/afdelingen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamer  $kamer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Afdeling::find($request->id)->delete();
        return back();
    }
}
