<?php

namespace App\Http\Controllers;

use App\Kamer;
use App\Locatie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KamersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$kamers = Kamer::all();
        /*$kamers = Kamer::with(['location' => function ($query) {
            $query->where('id', '=', Auth::user()->id);
        }])->get();*/
        /*$kamers = Kamer::with(array('location' => function($query)
        {
            $query->where('locaties.id', Auth::user()->id);
        }))->get();*/
        $kamers = Kamer::where('location_id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $kamers = Kamer::all();
        }
        //dd($kamers);
        return view('frontend.kamers.index', compact('kamers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locaties = Locatie::all();
        if($locaties->count() == 0){
            return redirect('/locaties')->with('error', "Gelieve eerst een <b>woonzorgcentrum</b> toe te voegen.");
        }
        return view('frontend.kamers.create', compact('locaties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Kamer::create($request->all());
        return redirect('/kamers');
    }

    public function edit(Kamer $kamer){
        return view('frontend.kamers.edit', compact('kamer'));
    }

    public function update(Request $request){
        $kamer = Kamer::find($request->id)->first();

        $kamercheck = Kamer::where('number', $request->number)->where('location_id', Auth::user()->location)->first();

        if($kamercheck){
            return back()->with('error', "Deze kamernaam bestaat al");
        }
        $kamer->number = $request->number;
        $kamer->save();

        return redirect('/kamers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamer  $kamer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Kamer::find($request->id)->delete();
        return back();
    }
}
