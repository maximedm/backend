<?php

namespace App\Http\Controllers;

use App\Bewoner;
use App\Hulpmiddel;
use App\Kamer;
use App\Locatie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BewonersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamers = Kamer::all();
        //$bewoners = Bewoner::all();

        $bewoners = DB::table('bewoners')->join('kamers', 'bewoners.room_id', '=', 'kamers.id')->join('locaties', 'kamers.location_id', '=', 'locaties.id')->select('bewoners.*', 'locaties.name')->where('locaties.id', Auth::user()->location)->get();        //dd($bewoners);

        if(Auth::user()->location == 0){
            $bewoners = Bewoner::all();
        }

        return view('frontend.bewoners.index', compact('bewoners', 'kamers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $locaties = Locatie::all();
        $hulpmiddelen = Hulpmiddel::all();
        $rooms = Kamer::where('location_id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $rooms = Kamer::all();
        }


        /*if($rooms->count() == 0){
            return redirect('/kamers')->with('error', "Gelieve eerst een <b>kamer</b> voor de bewoner toe te voegen.");
        }*/
        return view('frontend.bewoners.create', compact('locaties', 'hulpmiddelen', 'rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kamer = Kamer::where('number', $request->number)->where('location_id', Auth::user()->location)->first();

        if($kamer){
            if($kamer->bewoner){
                if($kamer->bewoner->id != $request->id){
                    return back()->with('error', 'kamer is al bewoond door ' . $kamer->bewoner->firstname . ' ' . $kamer->bewoner->lastname);
                }
            } else {
                //dd($kamer, 'Kamer is leeg maar bestaat al');
            }
        } else {
            //dd('Kamer is nog niet in gebruik');
            $kamer = new Kamer;
            $kamer->number = $request->number;
            $kamer->location_id = Auth::user()->location;
            $kamer->save();
        }

        //dd('passed all');

        $kamer = new Kamer;
        $kamer->number = $request->number;
        $kamer->location_id = Auth::user()->location;
        $kamer->save();

        //dd($request->all());
        $bewoner = new Bewoner;
        $bewoner->firstname = $request->firstname;
        $bewoner->lastname = $request->lastname;
        $bewoner->room_id = $kamer->id;
        $bewoner->breakfast = $request->breakfast;
        $bewoner->lunch = $request->lunch;
        $bewoner->dinner = $request->dinner;
        $bewoner->breakfast_hulpmiddelen = json_encode($request->breakfast_hulpmiddelen);
        $bewoner->lunch_hulpmiddelen = json_encode($request->lunch_hulpmiddelen);
        $bewoner->dinner_hulpmiddelen = json_encode($request->dinner_hulpmiddelen);
        $bewoner->breakfast_seat = $request->breakfast_seat;
        $bewoner->lunch_seat = $request->lunch_seat;
        $bewoner->dinner_seat = $request->dinner_seat;
        $bewoner->breakfast_notes = $request->breakfast_notes;
        $bewoner->lunch_notes = $request->lunch_notes;
        $bewoner->dinner_notes = $request->dinner_notes;
        $bewoner->save();

        return redirect('/bewoners');
    }

    public function edit(Bewoner $bewoner){
        $locaties = Locatie::all();
        $hulpmiddelen = Hulpmiddel::all();
        $rooms = Kamer::where('location_id', Auth::user()->location)->get();

        if(Auth::user()->location == 0){
            $rooms = Kamer::all();
        }

        return view('frontend.bewoners.edit', compact('bewoner', 'rooms', 'hulpmiddelen'));
    }

    public function update(Request $request){
        $kamer = Kamer::where('number', $request->number)->where('location_id', Auth::user()->location)->first();
        if($kamer){
            if($kamer->bewoner){
                if($kamer->bewoner->id != $request->id){
                    return back()->with('error', 'kamer is al bewoond door ' . $kamer->bewoner->firstname . ' ' . $kamer->bewoner->lastname);
                }
            } else {
                //dd($kamer, 'Kamer is leeg maar bestaat al');
            }
        } else {
            //dd('Kamer is nog niet in gebruik');
            $kamer = new Kamer;
            $kamer->number = $request->number;
            $kamer->location_id = Auth::user()->location;
            $kamer->save();
        }


        $bewoner = Bewoner::find($request->id)->first();
        $bewoner->firstname = $request->firstname;
        $bewoner->lastname = $request->lastname;
        $bewoner->room_id = $kamer->id;
        $bewoner->breakfast = $request->breakfast;
        $bewoner->lunch = $request->lunch;
        $bewoner->dinner = $request->dinner;
        $bewoner->breakfast_hulpmiddelen = json_encode($request->breakfast_hulpmiddelen);
        $bewoner->lunch_hulpmiddelen = json_encode($request->lunch_hulpmiddelen);
        $bewoner->dinner_hulpmiddelen = json_encode($request->dinner_hulpmiddelen);
        $bewoner->breakfast_seat = $request->breakfast_seat;
        $bewoner->lunch_seat = $request->lunch_seat;
        $bewoner->dinner_seat = $request->dinner_seat;
        $bewoner->breakfast_notes = $request->breakfast_notes;
        $bewoner->lunch_notes = $request->lunch_notes;
        $bewoner->dinner_notes = $request->dinner_notes;
        $bewoner->save();


        return redirect('/bewoners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamer  $kamer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Bewoner::find($request->id)->delete();
        return back();
    }
}
