<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBewonersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bewoners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path', 255)->nullable();
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->string('uuid', 100)->nullable();
            $table->unsignedBigInteger('room_id');
            $table->enum('breakfast', ['restaurant', 'kamer', 'eetruimte'])->default('eetruimte');
            $table->enum('lunch', ['restaurant', 'kamer', 'eetruimte'])->default('eetruimte');
            $table->enum('dinner', ['restaurant', 'kamer', 'eetruimte'])->default('eetruimte');
            $table->string('breakfast_hulpmiddelen')->nullable();
            $table->string('lunch_hulpmiddelen')->nullable();
            $table->string('dinner_hulpmiddelen')->nullable();
            $table->enum('breakfast_seat', ['rolstoel', 'leuningen', 'zonder'])->default('leuningen');
            $table->enum('lunch_seat', ['rolstoel', 'leuningen', 'zonder'])->default('leuningen');
            $table->enum('dinner_seat', ['rolstoel', 'leuningen', 'zonder'])->default('leuningen');
            $table->integer('table')->nullable();
            $table->longText('breakfast_notes')->nullable();
            $table->longText('lunch_notes')->nullable();
            $table->longText('dinner_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_id')->references('id')->on('kamers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bewoners');
    }
}
