<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'                => 0,
            'username'          => 'Administrator',
            'location'          => 0,
            'email'             => 'admin@happ.be',
            'password'          => bcrypt('Admin@123'),
            'password_user'     => bcrypt('Admin@123')
        ]);
    }
}

