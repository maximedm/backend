<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Overzicht woonzorgcentra</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/unslider.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/weather-icons/climacons.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/app.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- link(rel='stylesheet', type='text/css', href=app_assets_path+'/css'+rtl+'/pages/users.css')-->
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/selects/select2.min.css">


    <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
      data-open="click" data-menu="vertical-menu" data-col="2-columns">
<!-- fixed-top-->
<div id="app">
@include('partials.topbar')
<!-- ////////////////////////////////////////////////////////////////////////////-->
@include('partials.nav')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            @yield('content')
        </div>
    </div>
</div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2020 Zorgbedrijf Rivierenland. </span>
        <span class="float-md-right d-block d-md-inline-block d-none d-lg-block">Gemaakt met <i class="ft-heart pink"></i></span>
    </p>
</footer>
<!-- BEGIN VENDOR JS-->
<script src="/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="/app-assets/vendors/js/extensions/jquery.knob.min.js" type="text/javascript"></script>
<script src="/app-assets/js/scripts/extensions/knob.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js"
        type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js"
        type="text/javascript"></script>
<script src="/app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/charts/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/app-assets/css/core/colors/palette-climacon.css">
<link rel="stylesheet" type="text/css" href="/app-assets/fonts/simple-line-icons/style.min.css">
<!-- END PAGE VENDOR JS-->
<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"
        type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"
        type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/jszip.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/pdfmake.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/buttons.html5.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/buttons.print.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>

<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="/app-assets/js/core/app.js" type="text/javascript"></script>
<script src="/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"
        type="text/javascript"></script>
<script src="/app-assets/js/scripts/extensions/block-ui.js"></script>
<!-- END PAGE LEVEL JS-->
<!-- Laravel Scaffolding JS -->

@yield('scripts')
</body>
</html>
