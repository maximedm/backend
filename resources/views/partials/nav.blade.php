<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span>Algemeen</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
            </li>
            <li class="{{Request::is('/') ? 'open' : ''}} nav-item"><a href="/"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>

            </li>

            <li class="{{Request::is('bewoners*') ? 'open' : ''}} nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Bewoners</span></a>
                <ul class="menu-content">
                    <li class="{{Request::is('bewoners') ? 'active' : ''}} nav-item"><a href="/bewoners"><i class="fa fa-list-ul"></i><span class="menu-title" data-i18n="">Overzicht</span></a>
                    </li>
                    <li class="{{Request::is('bewoners/toevoegen*') ? 'active' : ''}} nav-item"><a href="/bewoners/toevoegen"><i class="ft-plus-square"></i><span class="menu-title" data-i18n="">Nieuwe bewoner toevoegen</span></a>
                    </li>
                </ul>
            </li>

            <li class="{{Request::is('kamers*') ? 'open' : ''}} nav-item"><a href="#"><i class="fa fa-bed"></i><span class="menu-title" data-i18n="">Kamers</span></a>
                <ul class="menu-content">
                    <li class="{{Request::is('kamers') ? 'active' : ''}} nav-item"><a href="/kamers"><i class="fa fa-list-ul"></i><span class="menu-title" data-i18n="">Overzicht</span></a>
                    </li>
                    <li class="{{Request::is('kamers/toevoegen*') ? 'active' : ''}} nav-item"><a href="/kamers/toevoegen"><i class="ft-plus-square"></i><span class="menu-title" data-i18n="">Nieuwe kamer toevoegen</span></a>
                    </li>
                </ul>
            </li>

            <li class="{{Request::is('hulpmiddelen*') ? 'open' : ''}} nav-item"><a href="#"><i class="fa fa-coffee"></i><span class="menu-title" data-i18n="">Hulpmiddelen</span></a>
                <ul class="menu-content">
                    <li class="{{Request::is('hulpmiddelen') ? 'active' : ''}} nav-item"><a href="/hulpmiddelen"><i class="fa fa-list-ul"></i><span class="menu-title" data-i18n="">Overzicht</span></a>
                    </li>
                    <li class="{{Request::is('hulpmiddelen/toevoegen*') ? 'active' : ''}} nav-item"><a href="/hulpmiddelen/toevoegen"><i class="ft-plus-square"></i><span class="menu-title" data-i18n="">Nieuw hulpmiddel toevoegen</span></a>
                    </li>
                </ul>
            </li>

            <li class=" navigation-header">
                <span>Ruimtelijke indeling</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
            </li>
            @if(Auth::user()->location == 0)
            <li class="{{Request::is('locaties*') ? 'open' : ''}} nav-item"><a href="/locaties"><i class="icon-home"></i><span class="menu-title" data-i18n="">Woonzorgcentra</span></a>
            {{--<ul class="menu-content">
                <li class=" nav-item"><a href="/locaties"><i class="fa fa-list-ul"></i><span class="menu-title" data-i18n="">Overzicht</span></a></li>
                <li class=" nav-item"><a href="/locaties/toevoegen"><i class="ft-plus-square"></i><span class="menu-title" data-i18n="">Nieuw woonzorgcentrum toevoegen</span></a></li>
                </ul>--}}
            </li>
            @endif
            <li class="{{Request::is('afdelingen*') ? 'open' : ''}} nav-item"><a href="/afdelingen"><i class="fa fa-sitemap"></i><span class="menu-title" data-i18n="">Afdelingen</span></a>
                {{--<ul class="menu-content">
                    <li class=" nav-item"><a href="/afdeling"><i class="fa fa-list-ul"></i><span class="menu-title" data-i18n="">Overzicht</span></a></li>
                    <li class=" nav-item"><a href="/afdeling/toevoegen"><i class="ft-plus-square"></i><span class="menu-title" data-i18n="">Nieuwe afdeling toevoegen</span></a></li>
                </ul>--}}
            </li>

            <li class="{{Request::is('ruimte*') ? 'open' : ''}} nav-item"><a href="/ruimte"><i class="fa fa-cutlery"></i><span class="menu-title" data-i18n="">Eetruimte's</span></a>
            </li>



            <li class=" navigation-header">
                <span>Overige</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Others"></i>
            </li>
            <li class=" nav-item"><a href="changelog.html"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Instellingen</span></a></li>
            <li class=" nav-item"><a href="#"><i class="ft-life-buoy"></i><span class="menu-title" data-i18n="">Hulp</span></a></li>
            <li class=" nav-item"><a href="/documentatie.html"><i class="ft-folder"></i><span class="menu-title" data-i18n="">Documentatie</span></a></li>
        </ul>
    </div>
</div>
