@extends('layouts.administratie')

@section('content')
    <!-- HTML5 export buttons table -->
    <section id="html5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Bewoners</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a href="/bewoners/toevoegen" class="btn btn-outline-primary"><i class="fa fa-plus"></i> Nieuwe bewoner toevoegen</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Woonzorcentrum</th>
                                    <th>Nr.</th>
                                    <th>Ontbijt</th>
                                    <th>Middageten</th>
                                    <th>Avondeten</th>
                                    <th>Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bewoners as $bewoner)
                                    <tr>
                                        <td>{{$bewoner->firstname}} {{$bewoner->lastname}}</td>
                                        @if(Auth::user()->location == 0)
                                            <td>{{$bewoner->room->location->name}}</td>
                                            @else
                                        <td>{{$bewoner->name}}</td>
                                        @endif
                                        <td>{{$bewoner->room_id}}</td>
                                        <td>{{$bewoner->breakfast}}</td>
                                        <td>{{$bewoner->lunch}}</td>
                                        <td>{{$bewoner->dinner}}</td>
                                        <td style="display: inline-flex">
                                            <a href="/bewoners/{{$bewoner->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                            <form action="/bewoners/delete" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{ $bewoner->id }}">
                                                <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Naam</th>
                                    <th>Woonzorcentrum</th>
                                    <th>Nr.</th>
                                    <th>Ontbijt</th>
                                    <th>Middageten</th>
                                    <th>Avondeten</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->
@endsection
