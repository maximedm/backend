@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        @if(Session::has('error'))
            <div class="alert bg-warning alert-icon-left alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Opmerking!</strong> {!! Session::get('error')!!}
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Bewoner toevoegen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/bewoners" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Bewoners overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>In dit formulier kan je een nieuwe bewoner toevoegen.</p>
                    </div>
                    <form class="form" method="POST" action="/bewoners/toevoegen">
                        {{csrf_field()}}
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie bewoner</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstname">Voornaam</label>
                                        <input type="text" id="firstname" class="form-control" placeholder="Voornaam" name="firstname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lastname">Achternaam</label>
                                        <input type="text" id="lastname" class="form-control" placeholder="Achternaam" name="lastname">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="room_id">Kamer nummer</label>
                                        {{--<select class="select2 kamer_nummer form-control" id="room_id" name="room_id" style="width:100%">
                                            @foreach($rooms as $room)
                                                <option value="{{$room->id}}" {{$room->bewoner ? 'disabled' : ''}}>{{$room->number}} {{$room->bewoner ? ' - Deze kamer is al in gebruik door ' . $room->bewoner->firstname . ' ' . $room->bewoner->lastname : ''}}</option>
                                            @endforeach
                                        </select>--}}
                                        <input type="text" id="number" class="form-control" placeholder="Kamer nummer" name="number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="path">Foto</label>
                                        <input type="file" id="path" class="form-control" placeholder="Foto" name="path">
                                    </div>
                                </div>
                            </div>
                            <h4 class="form-section"><i class="fa fa-paperclip"></i> Maaltijden</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="breakfast">Ontbijt</label>
                                        <select id="breakfast" name="breakfast" class="form-control">
                                            <option value="none" selected="" disabled="">Waar eet de bewoner deze maaltijd</option>
                                            <option value="kamer">Kamer</option>
                                            <option value="eetruimte">Eetruimte</option>
                                            <option value="restaurant">Restaurant</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lunch">Middag eten</label>
                                        <select id="lunch" name="lunch" class="form-control">
                                            <option value="0" selected="" disabled="">Waar eet de bewoner deze maaltijd</option>
                                            <option value="kamer">Kamer</option>
                                            <option value="eetruimte">Eetruimte</option>
                                            <option value="restaurant">Restaurant</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dinner">Avond eten</label>
                                        <select id="dinner" name="dinner" class="form-control">
                                            <option value="0" selected="" disabled="">Waar eet de bewoner deze maaltijd</option>
                                            <option value="kamer">Kamer</option>
                                            <option value="eetruimte">Eetruimte</option>
                                            <option value="restaurant">Restaurant</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hulpmiddelen_breakfast">Hulpmiddelen ontbijt</label>
                                        <select class="select2 form-control" id="breakfast_hulpmiddelen[]" name="breakfast_hulpmiddelen[]" multiple="multiple" style="width:100%">
                                            @foreach($hulpmiddelen as $hulpmiddel)
                                            <option value="{{$hulpmiddel->id}}">{{$hulpmiddel->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hulpmiddelen_lunch">Hulpmiddelen middag</label>
                                        <select class="select2 form-control" id="lunch_hulpmiddelen[]" name="lunch_hulpmiddelen[]" multiple="multiple" style="width:100%">
                                            @foreach($hulpmiddelen as $hulpmiddel)
                                                <option value="{{$hulpmiddel->id}}">{{$hulpmiddel->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="hulpmiddelen_dinner">Hulpmiddelen avond</label>
                                        <select class="select2 form-control" id="dinner_hulpmiddelen[]" name="dinner_hulpmiddelen[]" multiple="multiple" style="width:100%">
                                            @foreach($hulpmiddelen as $hulpmiddel)
                                                <option value="{{$hulpmiddel->id}}">{{$hulpmiddel->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="breakfast">Zithulpmiddel ontbijt</label>
                                        <select id="breakfast_seat" name="breakfast_seat" class="form-control">
                                            <option value="none" selected="" disabled="">Wat voor zithulpmiddel gebruikt de bewoner</option>
                                            <option value="rolstoel">Rolstoel</option>
                                            <option value="leuningen">Stoel met leuningen</option>
                                            <option value="zonder">Stoel zonder leuningen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lunch">Zithulpmiddel middag</label>
                                        <select id="lunch_seat" name="lunch_seat" class="form-control">
                                            <option value="none" selected="" disabled="">Wat voor zithulpmiddel gebruikt de bewoner</option>
                                            <option value="rolstoel">Rolstoel</option>
                                            <option value="leuningen">Stoel met leuningen</option>
                                            <option value="zonder">Stoel zonder leuningen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dinner">Zithulpmiddel avond</label>
                                        <select id="dinner_seat" name="dinner_seat" class="form-control">
                                            <option value="none" selected="" disabled="">Wat voor zithulpmiddel gebruikt de bewoner</option>
                                            <option value="rolstoel">Rolstoel</option>
                                            <option value="leuningen">Stoel met leuningen</option>
                                            <option value="zonder">Stoel zonder leuningen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="notes">Opmerkingen ontbijt (optioneel)</label>
                                        <textarea id="breakfast_notes" rows="5" class="form-control" name="breakfast_notes" placeholder="Opmerkingen bewoner"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="notes">Opmerkingen middag (optioneel)</label>
                                        <textarea id="lunch_notes" rows="5" class="form-control" name="lunch_notes" placeholder="Opmerkingen bewoner"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="notes">Opmerkingen avond (optioneel)</label>
                                        <textarea id="dinner_notes" rows="5" class="form-control" name="dinner_notes" placeholder="Opmerkingen bewoner"></textarea>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="table">Tafel nummer (optioneel)</label>
                                        <input type="text" id="table" class="form-control" placeholder="tafelnummer" name="table">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seat">Zithulpmiddel</label>
                                        <select id="seat" name="seat" class="form-control">
                                            <option value="none" selected="" disabled="">Wat voor zithulpmiddel gebruikt de bewoner</option>
                                            <option value="rolstoel">Rolstoel</option>
                                            <option value="leuningen">Stoel met leuningen</option>
                                            <option value="zonder">Stoel zonder leuningen</option>
                                        </select>
                                    </div>
                                </div>

                            </div>--}}

                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- end form -->
@endsection

@section('scripts')
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script>
        $(".select2").select2();
        $(".kamer_nummer").select2({placeholder: "Kies een kamer nummer"});

    </script>
@endsection
