@extends('layouts.administratie')

@section('content')
    <!-- HTML5 export buttons table -->
    <section id="html5">
        <div class="row">
            <!-- form -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Ruimte toevoegen</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                <p>In dit formulier kan je een nieuwe ruimte toevoegen.</p>
                            </div>
                            <form class="form" method="POST" action="/ruimte/toevoegen">
                                {{csrf_field()}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Naam ruimte</label>
                                                <input type="text" id="name" class="form-control" placeholder="Naam ruimte" name="name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2">Afdeling</label>
                                                <select name="locatie" id="" class="form-control">
                                                    <option disabled selected>Kies afdeling</option>
                                                    @foreach($afdelingen as $afdeling)
                                                        <option value="{{$afdeling->id}}">{{$afdeling->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> Opslaan
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end form -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Eetruimtes</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Afdeling</th>
                                    <th style="width: 15%">Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ruimtes as $ruimte)
                                    <tr>
                                        <td>{{$ruimte->name}}</td>
                                        @if(Auth::user()->location == 0)
                                            <td>{{$ruimte->afdeling->name}}</td>
                                            @else
                                        <td>{{$ruimte->afdelingname}}</td>
                                        @endif
                                        <td>
                                            <div class="form-inline">
                                                <a href="/ruimte/{{$ruimte->id}}/schets" class="btn btn-sm btn-outline-primary" style="margin-right:5px"><i class="fa fa-eye"></i> grondplan</a>
                                                <a href="/ruimte/{{$ruimte->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                                <form action="/afdeling/delete" method="post">
                                                    {{csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $afdeling->id }}">
                                                    <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                                </form>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Naam</th>
                                    <th>Afdeling</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->
@endsection
