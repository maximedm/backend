@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">{{$item->name}} wijzigen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/ruimte" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Ruimte overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p> In dit formulier kan je een ruimte aanpassen.</p>
                    </div>
                    <form class="form" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie woonzorgcentrum</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="number">Naam ruimte</label>
                                        <input type="text" id="name" class="form-control" placeholder="Naam ruimte" value="{{$item->name}}" name="name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="projectinput2">Afdeling</label>
                                        <select name="locatie" id="" class="form-control">
                                            <option disabled selected>Kies afdeling</option>
                                            @foreach($afdelingen as $afdeling)
                                                <option value="{{$afdeling->id}}" {{$afdeling->id == $item->department_id ? 'selected' : ''}}>{{$afdeling->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end form -->
@endsection
