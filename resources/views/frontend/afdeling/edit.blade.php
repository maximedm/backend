@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">{{$afdeling->name}} wijzigen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/afdelingen" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Afdeling overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p> In dit formulier kan je een afdeling aanpassen.</p>
                    </div>
                    <form class="form" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$afdeling->id}}">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie woonzorgcentrum</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="number">Afdeling naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Afdeling naam" value="{{$afdeling->name}}" name="name">
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Kamer referentie naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Kamer referentie naam" name="name">
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end form -->
@endsection
