@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Afdeling toevoegen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>In dit formulier kan je een nieuwe afdeling toevoegen.</p>
                    </div>
                    <form class="form" method="POST" action="/afdeling/toevoegen">
                        {{csrf_field()}}
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie afdeling</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Afdeling naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Afdeling naam" name="name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="projectinput2">Locatie</label>
                                        <select name="locatie" id="locatie" class="form-control">
                                            <option disabled selected>Locatie woonzorgcentrum</option>
                                            @foreach($locaties as $locatie)
                                                <option value="{{$locatie->id}}">{{$locatie->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end form -->
@endsection
