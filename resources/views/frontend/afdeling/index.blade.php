@extends('layouts.administratie')

@section('content')
    <!-- HTML5 export buttons table -->
    <section id="html5">
        @if(Session::has('error'))
            <div class="alert bg-warning alert-icon-left alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Opmerking!</strong> {!! Session::get('error') !!}
            </div>
        @endif
        <div class="row">
            <!-- form -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Afdeling toevoegen</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                <p>In dit formulier kan je een nieuwe afdeling toevoegen.</p>
                            </div>
                            <form class="form" method="POST" action="/afdeling/toevoegen">
                                {{csrf_field()}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Afdeling naam</label>
                                                <input type="text" id="name" class="form-control" placeholder="Afdeling naam" name="name">
                                            </div>
                                        </div>
                                        @if(\Auth::user()->location == 0)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2">Locatie</label>
                                                <select name="locatie" id="locatie" class="form-control">
                                                    <option disabled selected>Locatie woonzorgcentrum</option>
                                                    @foreach($locaties as $locatie)
                                                        <option value="{{$locatie->id}}">{{$locatie->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                            @endif
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> Opslaan
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end form -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Afdelingen</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">

                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Woonzorcentrum</th>
                                    <th style="width:10%">Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($afdelingen as $afdeling)
                                    <tr>
                                        <td>{{$afdeling->name}}</td>
                                        <td>{{$afdeling->location->name}}</td>
                                        <td style="display: inline-flex;">
                                            <a href="/afdeling/{{$afdeling->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                            <form action="/afdeling/delete" method="post">
                                                {{csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $afdeling->id }}">
                                                <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Naam</th>
                                    <th>Woonzorcentrum</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->
@endsection
