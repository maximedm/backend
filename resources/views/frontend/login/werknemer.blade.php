@extends('layouts.logins')

@section('content')
    <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    @if(Session::has('error'))
                        <div class="alert bg-warning alert-icon-left alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Probleem!</strong> {!! Session::get('error') !!}
                        </div>
                    @endif
                    <div class="card-header border-0">
                        <div class="card-title text-center">
                            <img src="/app-assets/images/logo/final_logo.png" style="width: 60%;padding: 5% 0;" alt="happ logo">
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                            <span>Logins</span>
                        </h6>
                    </div>
                    <div class="card-content">
                        <div class="card-body pt-0 text-center">
                            <a href="{{route('beheerder')}}" class="btn btn-outline-danger btn-block"><i class="ft-user"></i> Beheerders login</a>
                            <a href="{{route('werknemer')}}" class="btn btn-outline-primary btn-block"><i class="ft-user"></i> Werknemers login</a>
                        </div>
                        <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                            <span>Logins</span>
                        </p>
                        <div class="card-body">
                            <h3>Werknemers login</h3>
                            <form class="form-horizontal" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="location">Selecteer uw woonzorgcentrum</label>
                                    <select class="form-control" name="location" id="location">
                                        <option disabled selected>Selecteer uw woonzorgcentrum</option>
                                        @foreach($locaties as $locatie)
                                        <option value="{{$locatie->id}}">{{$locatie->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <fieldset class="form-group floating-label-form-group mb-1">
                                    <label for="user-password">Wachtwoord</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Wachtwoord">
                                </fieldset>
                                <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> Aanmelden</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
