@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">{{$hulpmiddel->name}} wijzigen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/hulpmiddelen" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Hulpmiddelen overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>In dit formulier kan je een hulpmiddel wijzigen.</p>
                    </div>
                    <form class="form" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$hulpmiddel->id}}">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie hulpmiddel</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Hulpmiddel naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Naam" value="{{$hulpmiddel->name}}" name="name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="path">Foto</label>
                                        <input type="file" id="path" class="form-control" placeholder="Foto" name="path">
                                        @if($hulpmiddel->path)<img src="/storage/{{ $hulpmiddel->path }}" alt="" style="width: 150px; height: 150px; object-fit: contain;">@endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Omschrijving hulpmiddel</label>
                                        <textarea id="description" rows="5" class="form-control" name="description" placeholder="Omschrijving van het hulpmiddel">{{$hulpmiddel->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>

                </form>
            </div>
        </div>
    </div>

    <!-- end form -->
@endsection
