@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Hulpmiddel toevoegen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/hulpmiddelen" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Hulpmiddelen overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>In dit formulier kan je een nieuw hulpmiddel toevoegen.</p>
                    </div>
                    <form class="form" method="POST" action="/hulpmiddelen/toevoegen" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie hulpmiddel</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Hulpmiddel naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Naam" name="name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="path">Foto</label>
                                        <input type="file" id="path" class="form-control" placeholder="Foto" name="path">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Omschrijving hulpmiddel</label>
                                        <textarea id="description" rows="5" class="form-control" name="description" placeholder="Omschrijving van het hulpmiddel"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- end form -->
@endsection
