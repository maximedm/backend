@extends('layouts.administratie')

@section('content')
    <!-- HTML5 export buttons table -->
    <section id="html5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Hulpmiddelen</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a href="/hulpmiddelen/toevoegen" class="btn btn-outline-primary"><i class="fa fa-plus"></i> Nieuw hulpmiddel toevoegen</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th style="width:10%">Foto</th>
                                    <th>Hulpmiddel</th>
                                    <th style="width:10%">Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($hulpmiddelen as $hulpmiddel)
                                    <tr>
                                        <td><img src="/storage/{{ $hulpmiddel->path }}" alt="" style="width: 150px; height: 150px; object-fit: contain;"></td>
                                        <td><h5>{{ $hulpmiddel->name }}</h5> {{$hulpmiddel->description}}</td>
                                        <td style="display: inline-flex">
                                            <a href="/hulpmiddelen/{{$hulpmiddel->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                            <form action="/hulpmiddelen/delete" method="post">
                                                {{csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $hulpmiddel->id }}">
                                                <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Foto</th>
                                    <th>Hulpmiddel</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->
@endsection
