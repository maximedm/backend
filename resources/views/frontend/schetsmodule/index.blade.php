@extends('layouts.administratie')

@section('content')
    <schets-module afdeling="{{$afdeling}}" eetruimte="{{$ruimte}}" woonzorgcentrum="{{$woonzorgcentrum}}"></schets-module>
{{--
    <iframe src="/schetsmodule" frameborder="0" style="width: 100%; height: 40vw;"></iframe>
--}}
@endsection
