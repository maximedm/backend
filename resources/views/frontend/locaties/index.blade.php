@extends('layouts.administratie')

@section('content')

    <!-- HTML5 export buttons table -->
    <section id="html5">
        @if(Session::has('error'))
            <div class="alert bg-warning alert-icon-left alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Opmerking!</strong> {!! Session::get('error')!!}
            </div>
        @endif
        <div class="row">
            <!-- form -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Woonzorgcentrum toevoegen</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                <p>In dit formulier kan je een nieuw woonzorgcentrum toevoegen.</p>
                            </div>
                            <form class="form" method="POST" action="/locaties">
                                {{csrf_field()}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Naam</label>
                                                <input type="text" id="name" name="name" class="form-control" placeholder="Naam van het woonzorgcentrum">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">Gemeente</label>
                                                <input type="text" id="city" name="city" class="form-control" placeholder="Gemeente van het woonzorgcentrum">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Admin wachtwoord</label>
                                                <input type="password" id="admin_password" name="admin_password" class="form-control" placeholder="Wachtwoord voor de administrator">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">Werknemer wachtwoord</label>
                                                <input type="password" id="user_password" name="user_password" class="form-control" placeholder="Wachtwoord voor de werknemer">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> Opslaan
                                </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end form -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Woonzorgcentra</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Gemeente</th>
                                    <th style="width:10%">Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($locaties as $locatie)
                                    <tr>
                                        <td>{{ $locatie->name }}</td>
                                        <td>{{ $locatie->city }}</td>
                                        <td style="display: inline-flex;">
                                            <a href="/locaties/{{$locatie->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                            <form action="/locaties/delete" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{ $locatie->id }}">
                                                <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Naam</th>
                                    <th>Gemeente</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->

@endsection
