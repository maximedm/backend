@extends('layouts.administratie')

@section('content')
    <!-- HTML5 export buttons table -->
    <section id="html5">
        @if(Session::has('error'))
            <div class="alert bg-warning alert-icon-left alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Opmerking!</strong> {!! Session::get('error') !!}
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Kamers</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a href="/kamers/toevoegen" class="btn btn-outline-primary"><i class="fa fa-plus"></i> Nieuwe kamer toevoegen</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered dataex-html5-export">
                                <thead>
                                <tr>
                                    <th>Kamer nummer</th>
                                    <th>Locatie</th>
                                    <th style="width:10%;">Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kamers as $kamer)
                                    <tr>
                                        <td>{{$kamer->number}}</td>
                                        <td>{{$kamer->location->name}}</td>
                                        <td style="display: inline-flex">
                                            <a href="/kamers/{{$kamer->id}}/update" class="btn btn-sm btn-outline-warning" style="margin-right:5px"><i class="fa fa-pencil"></i></a>
                                            <form action="/kamers/delete" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{ $kamer->id }}">
                                                <button type="submit" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Kamer nummer</th>
                                    <th>Locatie</th>
                                    <th>Acties</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ HTML5 export buttons table -->
@endsection


