@extends('layouts.administratie')

@section('content')
    <!-- form -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Kamer toevoegen</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a href="/kamers" class="btn btn-outline-primary"><i class="fa fa-list-ul"></i> Kamer overzicht</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>In dit formulier kan je een nieuwe kamer toevoegen.</p>
                    </div>
                    <form class="form" method="POST" action="/kamers/toevoegen">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-user"></i> Informatie kamer</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="number">Kamer nummer</label>
                                        <input type="text" id="number" class="form-control" placeholder="Kamer nummer" name="number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="projectinput2">Locatie</label>
                                        <select name="location_id" id="" class="form-control">
                                            <option disabled selected>Locatie woonzorgcentrum</option>
                                            @foreach($locaties as $locatie)
                                                <option value="{{$locatie->id}}">{{$locatie->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Kamer referentie naam</label>
                                        <input type="text" id="name" class="form-control" placeholder="Kamer referentie naam" name="name">
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Opslaan
                        </button>

                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- end form -->
@endsection
