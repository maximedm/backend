<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/werknemer', 'LoginsController@werknemer')->name('werknemer');
Route::post('/werknemer', 'LoginsController@werknemerLogin');
Route::get('/beheerder', 'LoginsController@beheerder')->name('beheerder');
Route::post('/beheerder', 'LoginsController@beheerderLogin');
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/beheerder');
});

Route::group( ['middleware' => 'auth' ], function()
{
    Route::get('/', 'DashboardController@index');
    Route::get('/locaties', 'LocatiesController@index');
    Route::post('/locaties', 'LocatiesController@store');
    Route::get('/locaties/{locatie}/update', 'LocatiesController@edit');
    Route::post('/locaties/{locatie}/update', 'LocatiesController@update');
    Route::post('/locaties/delete', 'LocatiesController@delete');


    Route::get('/kamers', 'KamersController@index');
    Route::get('/kamers/toevoegen', 'KamersController@create');
    Route::post('/kamers/toevoegen', 'KamersController@store');
    Route::get('/kamers/{kamer}/update', 'KamersController@edit');
    Route::post('/kamers/{kamer}/update', 'KamersController@update');
    Route::post('/kamers/delete', 'KamersController@delete');

//TBA
    Route::get('/kamers/{id}', 'KamersController@show');
    Route::post('/kamers/{id}', 'KamersController@update');



    Route::get('/bewoners', 'BewonersController@index');
    Route::get('/bewoners/toevoegen', 'BewonersController@create');
    Route::post('/bewoners/toevoegen', 'BewonersController@store');
    Route::get('/bewoners/{bewoner}/update', 'BewonersController@edit');
    Route::post('/bewoners/{bewoner}/update', 'BewonersController@update');
    Route::post('/bewoners/delete', 'BewonersController@delete');


//TBA
    Route::get('/bewoners/{id}', 'BewonersController@show');
    Route::post('/bewoners/{id}', 'BewonersController@update');



    Route::get('/hulpmiddelen', 'HulpmiddelenController@index');
    Route::get('/hulpmiddelen/toevoegen', 'HulpmiddelenController@create');
    Route::post('/hulpmiddelen/toevoegen', 'HulpmiddelenController@store');
    Route::get('/hulpmiddelen/{hulpmiddel}/update', 'HulpmiddelenController@edit');
    Route::post('/hulpmiddelen/{hulpmiddel}/update', 'HulpmiddelenController@update');
    Route::post('/hulpmiddelen/delete', 'HulpmiddelenController@delete');

//TBA
    Route::get('/hulpmiddelen/{id}', 'HulpmiddelenController@show');
    Route::post('/hulpmiddelen/{id}', 'HulpmiddelenController@update');



    Route::get('/ruimte', 'RuimteController@index');
    Route::get('/ruimte/toevoegen', 'RuimteController@create');
    Route::post('/ruimte/toevoegen', 'RuimteController@store');
    Route::post('/ruimte/delete', 'RuimteController@delete');
    Route::get('/ruimte/{ruimte}/schets', 'RuimteController@schets');


//TBA
    Route::get('/ruimte/{id}', 'RuimteController@show');
    Route::post('/ruimte/{id}', 'RuimteController@update');
    Route::get('/ruimte/{afdeling}/update', 'RuimteController@edit');
    Route::post('/ruimte/{ruimte}/update', 'RuimteController@updateEdit');



    Route::get('/afdelingen', 'AfdelingController@index');
    Route::get('/afdeling/toevoegen', 'AfdelingController@create');
    Route::post('/afdeling/toevoegen', 'AfdelingController@store');
    Route::get('/afdeling/{afdeling}/update', 'AfdelingController@edit');
    Route::post('/afdeling/{afdeling}/update', 'AfdelingController@update');
    Route::post('/afdeling/delete', 'AfdelingController@delete');

//TBA
    Route::get('/afdeling/{id}', 'AfdelingController@show');
    Route::post('/afdeling/{id}', 'AfdelingController@update');
});


Route::get('/api/bewoner/{bewoner}','ApiController@bewonerInfo');
Route::get('/api/locaties', 'ApiController@locaties');
Route::get('/api/bewoners/{locatie}', 'ApiController@bewoners');
Route::post('/api/bewoners/update', 'ApiController@updateUUID');
Route::get('/api/userlists/{locatie}', 'ApiController@userInfoList');
Route::get('/api/hulpmiddelen', 'ApiController@hulpmiddelen');
Route::get('/api/eetruimte/{eetruimte}', 'ApiController@eetruimteFind');
Route::post('/api/eetruimte/update', 'ApiController@eetruimteUpdate');
Route::get('/api/{locatie}/afdelingen', 'ApiController@afdelingenList');
Route::get('/api/{afdeling}/ruimtes', 'ApiController@ruimteList');

//Werknemers weergave
Route::get('/overzicht', 'LoginsController@overview');
Route::get('/indeling', 'LoginsController@indeling');
